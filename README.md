# Ragnyll's st build
Forked from https://git.suckless.org/st version 0.8.2

- build with `sudo make && sudo make install`

- custom color theme applied directly to `config.h`


## patches applied (in the patched order)
- [anysize](https://st.suckless.org/patches/anysize/st-anysize-0.8.1.diff)
- [alpha](https://st.suckless.org/patches/alpha/st-alpha-0.8.2.diff)
- [bold-is-not-bright](https://st.suckless.org/patches/bold-is-not-bright/st-bold-is-not-bright-20190127-3be4cf1.diff)

patches applied with `patch --merge -i (patch_name)`
